qsub -l mem_free=10G \
    -pe OpenMP 8 -v PATH -q short.q -l h_rt=2:0:0 \
    -o /mnt/projects/jjsalvarez/fluevolution/results/h3n2/$1.stdout -e $PWD/$1.stderr \
    -N $1 -wd $PWD \
    < $1
