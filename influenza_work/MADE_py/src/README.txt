runshort/medium/long allows running of scripts on the cluster on respective queues
use runshort.sh $1
$1 being the bash script that outlines the workflow needed for the datasets with hardcoded inputs

alignment_2508.sh
- MSA of H3N2 HA sequences (filtered >789bp using filterlength.sg in shortutils/)  using MUSCLE

alignment_01091.sh
- Redo of MSA of GISAID H3N2 HA sequences with the HA1 reference gene
