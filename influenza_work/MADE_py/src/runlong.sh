qsub -l mem_free=300G \
    -pe OpenMP 8 -v PATH -q long.q -l h_rt=480:0:0 \
    -o $PWD/$1.stdout -e $PWD/$1.stderr \
    -N $1 -wd $PWD \
    < $1

