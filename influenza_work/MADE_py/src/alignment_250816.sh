#!/bin/sh

# script to run alignment and queue large number of sequences in project folder

muscle -in /mnt/projects/jjsalvarez/fluevolution/h3n2/GISAID_H3N2HA_lenfiltered.fasta \
    -out /mnt/projects/jjsalvarez/fluevolution/h3n2/GISAID_H3N2HA_alignment.fasta -maxiters 2
