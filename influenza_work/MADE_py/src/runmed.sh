qsub -l mem_free=15G \
    -pe OpenMP 8 -v PATH -q medium.q -l h_rt=48:0:0 \
    -o $PWD/$1.stdout -e $PWD/$1.stderr \
    -N $1 -wd $PWD \
    < $1
