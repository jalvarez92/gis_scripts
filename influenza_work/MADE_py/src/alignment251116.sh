#!/bin/sh

# script to run alignment and queue large number of sequences in project folder

muscle -in /mnt/projects/jjsalvarez/fluevolution/data/h3n2/GISAID_H3N2_prealign.fasta \
    -out /mnt/projects/jjsalvarez/fluevolution/results/h3n2/GISAID_H3N2HA_alignment_dec1.fasta -maxiters 2
