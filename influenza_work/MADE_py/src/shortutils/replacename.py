#!/usr/bin/env python

fasta= open('copy.fas')
newnames= open('newIDs.txt')
newfasta= open('replaced.fas', 'w')

for line in fasta:
    if line.startswith('>'):
        newname= newnames.readline()
        newfasta.write('>' + newname)
    else:
        newfasta.write(line)

fasta.close()
newnames.close()
newfasta.close()
