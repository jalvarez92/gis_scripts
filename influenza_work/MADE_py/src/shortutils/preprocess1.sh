#remove spaces/'>' or anything else in headers
#repalce 2nd character with char of interest
sed 's,_>,_,g' -i inplacechange.fasta

#remove duplicates
awk 'BEGIN{RS=">"}NR>1{sub("\n","\t"); gsub("\n",""); print RS$0}' in.fasta | awk '!seen[$1]++' | awk -v OFS="\n" '{print $1,$2}' > out.fasta
