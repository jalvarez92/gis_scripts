#!/bin/sh

# remove spaces in header
sed 's, ,_,g' -i GISAID_H3N2HA.fasta > try.fa

# remove lnngths less than 987
bioawk -c fastx '{ if(length($seq) > 986 { print ">"$name; print $seq }}' GISAID_H3N2HA.fasta > try.fa


echo 'Done!'

