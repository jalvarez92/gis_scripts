file_in = open("wrap_test.fa")

file_out = open("wrap_test_out.fa", "a")

final = {}

for line in file_in:
    line = line.strip()
    if line.startswith(">"):
        id = line
        final[id] = " "
    else:
        final[id] += line


for kee, val in final.items():
    file_out.write(kee)
    file_out.write("\n")
    file_out.write(val)
    file_out.write("\n")
