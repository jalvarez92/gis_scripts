#!/bin/bash
#Realignment of GISAID sequences with HA reference gene

muscle -in /mnt/projects/jjsalvarez/fluevolution/data/h3n2/GISAID_H3N2_HA1_987bp.fasta \
    -out /mnt/projects/jjsalvarez/fluevolution/results/h3n2/GISAID_H3N2HA1_alignment.fasta -maxiters 2
